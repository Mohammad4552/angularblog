import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ArticleComponent } from './article/article.component';
import { SingleArticleComponent } from './single-article/single-article.component';

const routes: Routes = [{path:'',component:ArticleComponent},
{path:'article/:id',component:SingleArticleComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
