import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Article } from './entities';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {


  constructor (private http: HttpClient) { }

  getAllSimpleArticle(){
    return this.http.get<Article[]>(environment.apiUrl+'/api/article');
  }

  getArticleById(id:number){     
    return this.http.get<Article>(environment.apiUrl+'/api/article/'+id);  
  }

  getArticleByIdWithComment(id:number){
    return this.http.get<Article>(environment.apiUrl+'/api/article/'+id);
  }
  deleteArticleById(id:number){
    return this.http.delete<Article>(environment.apiUrl+'/api/article/'+id);
  }

  addArticle(article:Article){
    return this.http.post<Article>(environment.apiUrl+'/api/article/',article);

  }
  
  

}
