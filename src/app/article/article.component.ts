import { Component, OnInit } from '@angular/core';
import { ArticleService } from '../article.service';
import { Article } from '../entities';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {
  article?: Article;
  articles?:Article[];
  articleToCreate:Article={
    id:0,
    title:'',
    text:'',
    image:'',
    date:''
  }

  constructor(private artService: ArticleService) { }
  getArticleById(id:number){
    this.artService.getArticleById(id).subscribe((data: any)=>this.article=data)
  }
  getAllArticle(){
    this.artService.getAllSimpleArticle().subscribe(data=>this.articles=data);
  }
  addArticle(){
    this.artService.addArticle(this.articleToCreate).subscribe();
  }

  ngOnInit(): void {
    this.getAllArticle();
  }

}
