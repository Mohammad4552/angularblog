export interface Article{
id: number,
title: string,
text: string,
image:string,
date: string
}

export interface Comments{
    id: number,
    text: string
}