import { Component, OnInit } from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import { switchMap } from 'rxjs';
import { ArticleService } from '../article.service';
import { Article } from '../entities';

@Component({
  selector: 'app-single-article',
  templateUrl: './single-article.component.html',
  styleUrls: ['./single-article.component.css']
})
export class SingleArticleComponent implements OnInit {

  constructor(private service: ArticleService, private route: ActivatedRoute) { }

  article?: Article;
  deleteArticle(id:number){
    console.log(this.article?.id);
    this.service.deleteArticleById(id).subscribe();
  }
  ngOnInit(): void {
    this.route.params.pipe(
      switchMap(params => this.service.getArticleById(params['id']))
    )
      .subscribe(data => this.article = data);
  }

}


